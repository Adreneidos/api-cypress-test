import { Given, Then } from "cypress-cucumber-preprocessor/steps";

Given ('I send success GET to randomuser', () => {
    cy.request({
        method: 'GET',
        url: 'https://randomuser.me/api/',
        failOnStatusCode: false,
    }).then(
        (response) => {
          expect(response.status).to.eq(200)
          expect(response.body).to.have.property('results') 
        }
      )
})


