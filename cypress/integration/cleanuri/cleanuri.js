import { Given, Then } from "cypress-cucumber-preprocessor/steps";


Given ('I send success POST to cleanuri', () => {
    cy.request({
        method: 'POST',
        url: 'https://cleanuri.com/api/v1/shorten',
        failOnStatusCode: false,
        body: Cypress.env("correctUrl")
        
    }).then(
  (response) => {
    expect(response.body).to.have.property('result_url', 'https://cleanuri.com/Bpne2X') 
  }
)
})


Then ('I send wrong POST to cleanuri', () => {
    cy.request({
      method: 'POST',
      url: 'https://cleanuri.com/api/v1/shorten',
      failOnStatusCode: false,
      body: Cypress.env("invalidUrl")
    
    }).then(
  (response) => {
    expect(response.status).to.eq(400)
    expect(response.body).to.have.property('error', 'API Error: URL is invalid (check #1)') 
  }
)
})

Then ('I send second wrong POST to cleanuri', () => {
    cy.request({
      method: 'POST',
      url: 'https://cleanuri.com/api/v1/shorten',
      failOnStatusCode: false,
      body: Cypress.env("notCorrectUrl")
    
    }).then(
  (response) => {
    expect(response.status).to.eq(400)
    expect(response.body).to.have.property('error', 'API Error: This domain blocked in our system') 
  }
)
})